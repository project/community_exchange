<?php

namespace Drupal\community_exchange\Access;

use Drupal\user\UserAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Show user profiles only to users in the same exchange.
 *
 * @todo move this to the Group module or maybe mcapi_exchanges module
 * @todo give the user a privacy flag
 */
class UserExchangeAccessControlHandler extends UserAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $result = parent::checkAccess($entity, $operation, $account);

    if ($result->isAllowed() && $entity->id() != $account->id() && !$account->hasPermission('administer users')) {
      // Find out which exchange this user is in.
      foreach (\Drupal::service('group.membership_loader')->loadByUser($entity) as $membership) {
        $group = $membership->getGroup();
        if ($group->getGroupType()->id() == 'exchange') {
          if ($group->id() != mcapi_exchanges_current_membership()->getGroup()->id()) {
            drupal_set_message('Mem of exchange '. mcapi_exchanges_current_membership()->getGroup()->id() ." barred from $operation members in exchange ".$group->id());
            return $result->forbidden();
          }
        }
      }
    }
    return $result;
  }

}
