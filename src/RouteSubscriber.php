<?php

namespace Drupal\community_exchange;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alter routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   *
   * Anything the committee does shouldn't be an admin_route.
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Prevent group owners from changing available content types
    if ($route = $collection->get('entity.group_type.content_plugins')) {
      $route->setRequirement('_permission', 'bypass group access');
    }

    // This views default tab seems to be showing to anon despite none of its
    // children being accessible
    $collection->get('view.smallads_directory.page_offers')
      ->setRequirements(['_user_is_logged_in' => 'true', '_method' => 'GET|POST']);
  }

}
