<?php

namespace Drupal\community_exchange;

use Drupal\Core\PathProcessor\PathProcessorFront as CorePathProcessorFront;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes the inbound path by resolving it to the front page if empty.
 *
 * Overrides
 *
 * @todo - remove ::processOutbound() when we remove UrlGenerator::fromPath().
 */
class PathProcessorFront extends CorePathProcessorFront {

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    if ($path === '/') {
      if ($membership = group_exclusive_membership_get('exchange')) {
        // This isn't the proper way to get the canonical path
        $path = '/group/'.$membership->getGroup()->id();
      }
      else {
        $path = parent::processInbound($path, $request);
      }
    }
    return $path;
  }
}
