<?php

namespace Drupal\community_exchange;

use Drupal\group\Entity\GroupContent;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Installation options
 */
class Setup extends FormBase {

  private $installer;
  private $accountSwitcher;

  /**
   * Constructor.
   */
  public function __construct($module_installer, $account_switcher) {
    $this->installer = $module_installer;
    $this->accountSwitcher = $account_switcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_installer'),
      $container->get('account_switcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eserai_setup_options';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->installer->install(['devel_generate']);
    $form['#title'] = $this->t('Install demo data');
    $form['demodata'] = [
      'users' => [
        '#title' => t('Users'),
        '#type' => 'number',
        '#default_value' => 0,
        '#max' => 2000
      ],
      'offers' => [
        '#title' => t('Offers'),
        '#type' => 'number',
        '#default_value' => 0,
        '#max' => 5000
      ],
      'wants' => [
        '#title' => t('Wants'),
        '#type' => 'number',
        '#default_value' => 0,
        '#max' => 5000
      ],
      'wallposts' => [
        '#title' => t('Wall posts'),
        '#type' => 'number',
        '#default_value' => 0,
        '#max' => 5000
      ],
      'exchanges' => [
        '#title' => t('Exchanges'),
        '#type' => 'number',
        '#default_value' => 1,
        '#min' => 1,
        '#max' => 100,
        //TEMP
        '#disabled' => !class_exists('\Drupal\group\Plugin\DevelGenerate\GroupDevelGenerate')
      ],
      'transactions' => [
        '#title' => t('Transactions per exchange'),
        '#description' => t('Between users in the same exchanges'),
        '#type' => 'number',
        '#default_value' => 0,
        '#max' => 5000
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'submit',
      '#weight' => 10,
    ];
    return $form;
  }

  /**
   * Submit callback
   *
   * We have to generate all this data in the right order. Content must be
   * created first, then added to groups. Transactions can only be created after
   * users are in groups.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal\user\Entity\Role::load('authenticated')->grantPermission('switch users')->save();

    $batch = [];
    if ($num = $form_state->getValue('exchanges')){
      $operations[] = [
        'generate_content',
        [
          'exchange_neighbourhoods',
          [
            'kill' => FALSE,
            'num' => $num,
            'neighbourhoods' => 4
          ]
        ]
      ];
    }
    if ($num = $form_state->getValue('users')){
      $operations[] = [
        'generate_content',
        [
          'user',
          [
            'kill' => FALSE,
            'num' => $num,
            'pass' => 'password',
            'roles' => [],
            'time_range' => strtotime('-2 years'),
          ]
        ]
      ];
      // Resave the users with the same country as their respective exchanges.
      $operations[] = [
        '\Drupal\community_exchange\Setup::resaveAllUsers', []
      ];
    }
    // Put all the users into random groups.
    $operations[] = [
      'generate_content',
      [
        'group_content',
        [
          'kill' => FALSE,
          'group_content_types' => ['exchange-group_membership'],
        ],
      ],
    ];
    // Put all the users into random groups.
    $operations[] = [
      'generate_content',
      [
        'group_content',
        [
          'kill' => FALSE,
          'group_content_types' => ['neighbourhood-group_membership'],
        ],
      ],
    ];
    // Remove user1 group memberships after all users have been added to groups
    // and before creating transactions between members of exchanges.
    $operations[] = ['\Drupal\community_exchange\Setup::removeUser1', []];

    if ($num = $form_state->getValue('wallposts')){
      $operations[] = [
        'generate_content',
        [
          'content',
          [
            'kill' => FALSE,
            'num' => $num,
            'node_types' => ['post' => TRUE],
            'max_comments' => 2,
            'title_length' => 8,
          ],
        ],
      ];
    }
    //this is not a form option
    $operations[] = [
      'generate_content',
      [
        'term',
        [
          'kill' => FALSE,
          'num' => 10*$form_state->getValue('exchanges'),
          'vids' => ['categories'],
          'title_length' => 13
        ],
      ],
    ];

    if ($num = $form_state->getValue('offers')) {
      $operations[] = [
        'generate_content',
        [
          'smallad',
          [
            'kill' => FALSE,
            'num' => $num,
            'type' => 'offer',
            'since' => strtotime('-6 months'),
          ],
        ],
      ];
    }
    if ($num = $form_state->getValue('wants')) {
      $operations[] = [
        'generate_content',
        [
          'smallad',
          [
            'kill' => FALSE,
            'num' => $num,
            'type' => 'want',
            'since' => strtotime('-6 months'),
          ],
        ],
      ];
    }

    // Create transactions between members in the same groups.
    // Has to happen after user 1 has been removed from groups
    $operations[] = [
      'generate_content',
      [
        'mcapi_exchange_transaction',
        [
          'kill' => FALSE,
          'type' => 'default',
          'num' => $form_state->getValue('transactions'),
        ],
      ],
    ];
    $operations[] = [
      'generate_content',
      [
        'group_content',
        [
          'kill' => FALSE,
          'group_content_types' => [
            'exchange-group_node-post',
            'exchange-group_term-categories',
            'exchange-smallad-offer',
            'exchange-smallad-want'
          ],
        ],
      ],
    ];

    if ($operations) {
      $batch = [
        'title' => t('Generating example content and putting in groups'),
        'operations' => $operations,
        'file' => 'profiles/community_exchange/community_exchange.install',
        'finished' => '\Drupal\community_exchange\Setup::finish'
      ];
      global $install_state;
      $install_state['eserai_batch'] = $batch;
    }
    else {
      $this->installer->uninstall(['devel_generate']);
    }
  }

  /**
   * Remove users 0 & 1 from memberships from all groups, replacing group owner
   */
  static function removeUser1() {
    $membership_ids = \Drupal::entityQuery('group_content')
      ->condition('type', ['exchange-group_membership', 'neighbourhood-group_membership'], 'IN')
      ->condition('entity_id', [0, 1], 'IN')
      ->execute();
    \Drupal::logger('eserai')->info(
      'Deleting @num user 1 memberships from exchanges and neighbourhoods',
      ['@num' => count($membership_ids)]
    );
    foreach (GroupContent::loadMultiple($membership_ids) as $mem) {
      $mem->delete();
    }
    // Make the first membership of each exchange the owner of the exchange and
    // neighbourhoods
    $exchanges = \Drupal::entityTypeManager()
      ->getStorage('group')
      ->loadByProperties(['type' => 'exchange']);
    foreach ($exchanges as $exchange_group) {
      $memberships = $exchange_group->getMembers();
      if (empty($memberships)) {
         throw new \Exception('Stopping because no members in exchange '.$exchange_group->id());
      }
      $new_owner_membership = reset($memberships);
      static::makeGroupOwnerAndAdmin($exchange_group, $new_owner_membership, 'exchange-admin');

      $neighbourhood_content_ids = \Drupal::entityQuery('group_content')
        ->condition('gid', $exchange_group->id())
        ->condition('type', 'exchange-subgroup-neighbourhood')
        ->execute();
      foreach (GroupContent::loadMultiple($neighbourhood_content_ids) as $hood_content) {
        $memships = \Drupal::service('group.membership_loader')
          ->loadByGroup($hood_content->getEntity());
        if ($memship = reset($memships)) {
          static::makeGroupOwnerAndAdmin($hood_content->getEntity(), $memship, 'neighbourhood-super');
        }
        else {
          drupal_set_message(
            t(
              "Neighbourhood group @gid has no memberships",
              [
                '@exid' => $exchange_group->id(),
                '@uid' => $new_owner_membership->getUser()->id(),
                '@gid' => $hood_content->getEntity()->id()
              ]
            )
          );
        }
      }
    }

  }

  public static function makeGroupOwnerAndAdmin($group, $membership, $role_id) {
    $account = $membership->getUser();
    $group->setOwner($account)->save();
    $roles = $membership->getRoles();
    unset($roles['exchange-member'], $roles['neighbourhood-member']);
    $roles = array_merge(array_keys($roles), [$role_id]);

    $membership->getGroupContent()->set('group_roles', array_unique($roles))->save();
    \Drupal::logger('eserai')
      ->info(
        'Changed owner of @group_type @group_id to user @uid and granted roles @roles',
        ['@group_type' => $group->bundle(),
          '@group_id' => $group->id(),
          '@uid' => $account->id(),
          '@roles' => implode(', ', $roles)
        ]
      );
  }

  /**
   * Set each user's country to their parent exchange - just resave them and let
   * community_exchange_user_presave do the work.
   */
  function resaveAllUsers() {
    foreach (User::loadMultiple() as $user) {
      $user->save();
    }
  }

  /**
   * Batch finished callback
   */
  static function finish() {
    // @todo also uninstall devel.
    \Drupal::service('module_installer')->uninstall(['devel_generate']);
  }

}


