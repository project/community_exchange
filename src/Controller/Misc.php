<?php

namespace Drupal\community_exchange\Controller;

use \Drupal\system\Controller\SystemController;

/**
 * Displays the text of the readme file for the given module.
 */
class Misc extends SystemController {

  /**
   * Page callback.
   */
  public function childLinks() {
    $link = \Drupal::service('menu.active_trail')->getActiveLink('main');

    if ($link && $content = \Drupal::service('system.manager')->getAdminBlock($link)) {
      return [
        '#theme' => 'admin_block_content',
        '#content' => $content,
      ];
    }
    else {
      return [
        '#markup' => $this->t('No links here.')
      ];
    }
  }

}
