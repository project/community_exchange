<?php

/**
 * @file
 * Contains \Drupal\community_exchange\Controller\ExchangeContactController.
 */

namespace Drupal\community_exchange\Controller;

use Drupal\contact\Controller\ContactController;
use Drupal\group\Entity\GroupInterface;

/**
 * Controller routines for contact routes.
 */
class ExchangeController extends ContactController {

  /**
   * Form constructor for the personal contact form.
   *
   * @param GroupInterface $exchange
   *   The exchange to be contacted.
   *
   * @return array
   *   A render array.
   */
  public function page(GroupInterface $exchange) {
    // Check if flood control has been activated for sending emails.
    if (!$this->currentUser()->hasPermission('administer contact forms') && !$this->currentUser()->hasPermission('administer users')) {
      $this->contactFloodControl();
    }
    //@todo hope the contact module evolves a bit, or build our own contact form
    $message = $this->entityTypeManager()->getStorage('contact_message')->create([
      'category' => 'exchange',
    ]);
    drupal_set_message("Contact form isn't working yet");

    $form = $this->entityFormBuilder()->getForm($message);
    $form['#title'] = $this->t('Contact @exchangename', array('@exchangename' => $exchange->label()));
    return $form;
  }

}
