<?php

/**
 * @file
 */

namespace Drupal\community_exchange\Controller;

use Drupal\user\Entity\User;
use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for 'me' links to user FormDisplays.
 * @todo maybe replace when 'me' module is done.
 * @see http://drupal.org/project/me
 * @see community_exchange.routing.yml
 * @see community_exchange.links.menu.yml
 */
class UserController extends ControllerBase {

  /**
   * Redirects users to their settings page.
   *
   * This controller assumes that it is only invoked for authenticated users.
   * This is enforced for the 'user.page' route with the '_user_is_logged_in'
   * requirement.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a redirect to the profile of the currently logged in user.
   */
  public function userProfilePage() {
    return $this->redirect('entity.user.editform_default', ['user' => $this->currentUser()->id()]);
  }
  public function userSettingsPage() {
    return $this->redirect('entity.user.settings', ['user' => $this->currentUser()->id()]);
  }
  public function userSmallads() {
    return $this->redirect('view.smallads_user.page_1', ['user' => $this->currentUser()->id()]);
  }
  public function userTransactions() {
    return $this->redirect('view.wallet_transactions.user', ['user' => $this->currentUser()->id()]);
  }

  /**
   *
   * @param User $user
   *   The user from the route.
   *
   * @return array
   *   A renderable array
   *
   * @note the route is not enhanced  see Drupal\Core\Entity\Enhancer\EntityRouteEnhancer::applies
   *
   * @todo inject UserViewBuilder
   */
  public function recommendations($user) {
    return \Drupal::entityTypeManager()->getViewBuilder('user')->view($user, 'comments');
  }

  /**
   *
   * @param User $user
   *   The user from the route.
   *
   * @return string
   *   The translated page title.
   *
   * @note the route is not enhanced  see Drupal\Core\Entity\Enhancer\EntityRouteEnhancer::applies
   */
  public function recommendationsTitle($user) {
    return $this->t("%name's recommendations", ['%name' => $user->getDisplayName()]);
  }
}
