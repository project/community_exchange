<?php

namespace Drupal\community_exchange\Plugin\Block;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a block to display 'Site branding' elements.
 *
 * @Block(
 *   id = "subgroups",
 *   admin_label = @Translation("Sub groups")
 * )
 * @todo move this to the subgroup module and configure the contest to get the
 * group from
 */
class Subgroups extends BlockBase {

  /**
   * The exchange from context, i.e. from the current user if not, the path.
   *
   * @var \Drupal\group\Entity\Group
   */
  private $exchange;

  /**
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param array $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    if ($membership = group_exclusive_membership_get('exchange')) {
      $this->exchange = $membership->getGroup();
    }
  }


  /**
   * {@inheritdoc}
   */
  public function build() {
    if (!$this->exchange) {
      drupal_set_message('caching problem with ExchangeBrandingBlock', 'error');
      return [];
    }
    return ggroup_build_subgroup_list($this->exchange, $this->configuration['sub_group_types']);
  }

  /**
   * {@inheritdoc}
   */
  function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIf(!empty($this->exchange))->cachePerUser();
  }


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'context' => NULL,
      'sub_group_types' => []
    ];
  }


  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $types = \Drupal\group\Entity\GroupContentType::loadByEntityTypeId('group');
    foreach ($types as $id => $type) {
      $options[$id] = $type->label();
    }

    $form['sub_group_types'] = [
      '#title' => $this->t('Sub-group types'),
      '#description' => $this->t('Select none to select all'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->configuration['sub_group_types']
    ];
    return $form;
  }

  /**
   *
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    //Save the settings?
    $this->setConfigurationValue(
      'sub_group_types',
      array_filter(array_values($form_state->getValue('sub_group_types')))
    );
  }
}
