<?php

namespace Drupal\community_exchange\Plugin\Block;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a block to display 'Site branding' elements.
 *
 * @Block(
 *   id = "community_exchange_branding",
 *   admin_label = @Translation("Site branding per exchange")
 * )
 */
class ExchangeBrandingBlock extends BlockBase {

  /**
   * The exchange from context, i.e. from the current user if not, the path.
   *
   * @var \Drupal\group\Entity\Group
   */
  private $exchange;

  /**
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param array $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    if ($membership = group_exclusive_membership_get('exchange')) {
      $this->exchange = $membership->getGroup();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (!$this->exchange) {
      drupal_set_message('caching problem with ExchangeBrandingBlock', 'error');
      return [];
    }

    $build = array();
    if ($logo_file = $this->exchange->logo->entity) {
      // @todo why isn't this linking to the home page?
      $build['site_logo'] = [
        '#theme' => 'image_style',
        '#style_name' => 'thumbnail',
        '#uri' => $logo_file->uri->value,//path from drupal root
        '#alt' => $this->t('Home'),
        '#prefix' => '<a href="\">',
        '#suffix' => '</a>',
      ];
    }

    $build['site_name'] = [
      '#markup' => '<h1>'.$this->exchange->label().'</h1>',
        '#prefix' => '<a href="\">',
        '#suffix' => '</a>',
    ];
    //this leverages existing css in bartik
    $build['#attributes']['class'][] = 'site-branding';
    $build['#attributes']['class'][] = 'site-branding__name';

    $build['#attached']['library'][] = 'community_exchange/default';

    $build['site_slogan'] = [
      '#markup' => '<p>'.$this->exchange->slogan->value.'</p>' ?: ''
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIf(!empty($this->exchange))->cachePerUser();
  }

}
