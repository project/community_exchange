<?php

namespace Drupal\community_exchange\Plugin\DevelGenerate;

use Drupal\mcapi_exchanges\Plugin\DevelGenerate\ExchangeGenerate;
use Drupal\group\Entity\Group;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Generate neighbourhoods in all existing exchanges
 *
 * @DevelGenerate(
 *   id = "exchange_neighbourhoods",
 *   label = @Translation("Exchanges with neighbourhoods"),
 *   description = @Translation("Replaces Generate Exchanges"),
 *   url = "exchange",
 *   permission = "administer devel_generate",
 *   settings = {
 *     "num" = 5,
 *     "kill" = TRUE
 *   }
 * )
 *
 * @todo this will need updating if we use subgroups
 *
 */
class ExchangeNeighbourhoodsGenerate extends ExchangeGenerate implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['neighbourhoods'] = [
      '#type' => 'number',
      '#title' => $this->t('How many neighbourhoods per exchange would you like to generate on average?'),
      '#default_value' => $this->getSetting('num'),
      '#required' => TRUE,
    ];
    return $form;
  }


  /**
   * Deletes all groups of the given type .
   *
   * @param array $values
   *   The input values from the settings form.
   */
  public function groupsKill($values) {
    $values['group_types'][] = 'neighbourhood';
    parent::groupsKill($values);
  }

  /**
   * Create an exchange with neighbourhoods
   */
  protected function addExchange($values) {
    // Created by a random user
    $values['uid'] = $values['users'][array_rand($values['users'])];
    parent::addExchange($values);
    // Now add some neighbourhoods
    $exchange = $this->lastExchange();
    // Recover the exchange for some final alterations.
    $diff = floor($values['neighbourhoods'] / 2);
    $num = rand($diff, $values['neighbourhoods'] + $diff);
    for($i = 0; $i < $num; $i++) {
      $props = [
        'label' => $this->randomHoodName(),
        'uid' => $values['uid'],
        'type' => 'neighbourhood',
      ];
      $neighbourhood = Group::create($props);
      $neighbourhood->save();
      // Make the exchange owner the super-neighbour
      $this->groupMembershipLoader
        ->load($neighbourhood, $neighbourhood->getOwner())
        ->getGroupContent()
        ->set('group_roles', ['neighbourhood-super'])
        ->save();
      $this->logger->notice(
        'Granted super-neighbour role to user @owner_id, owner of neighbourhood @hood',
        ['@owner_id' => $neighbourhood->getOwnerId(), '@hood' => $neighbourhood->label()]
      );
      // Add the neighbourhood as content to the $exchange
      $exchange->addContent($neighbourhood, 'subgroup:neighbourhood', ['uid' => $exchange->getOwnerId()]);
    }
  }

  private function randomHoodName() {
    $first = ['Lonesome', 'Twin', 'Triple', 'Square', 'Five', 'Green', 'Dark', 'Cross', 'Cherry'];
    $second = ['Canyon', 'Parks', 'Posts', 'Ponds', 'Brooks', 'Hamlets', 'Roads', 'Gully', 'Side', 'Pines'];
    return $first[array_rand($first)] .' '.$second[array_rand($second)];
  }

}

