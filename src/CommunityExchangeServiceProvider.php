<?php

namespace Drupal\community_exchange;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Overrides default services.
 */
class CommunityExchangeServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Overrides language_manager class to test domain language negotiation.
    $definition = $container->getDefinition('path_processor_front')
      ->setClass('Drupal\community_exchange\PathProcessorFront');
  }

}
