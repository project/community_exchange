<?php

/**
 * @file
 * hooks for group_exclusive module
 */
use Drupal\mcapi_exchanges\Exchanges;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\Group;

/**
 * Implements hook_entity_type_alter().
 *
 * Adds extra FormDisplays to the user entity.
 */
function community_exchange_entity_type_alter(array &$entity_types) {
  $entity_types['user']->setFormClass('settings', "Drupal\user\ProfileForm");
  $entity_types['user']->setFormClass('profile', "Drupal\user\ProfileForm");
  $entity_types['user']->setAccessClass('Drupal\community_exchange\Access\UserExchangeAccessControlHandler');
  if (isset($entity_types['group'])) {
    $entity_types['group']->setFormClass('templates', "Drupal\group\Entity\Form\GroupForm");
    if (\Drupal::moduleHandler()->moduleExists('contact')) {
      $entity_types['group']->setFormClass('contact', 'Drupal\community_exchange\Form\Contact');
      $entity_types['group']->setFormClass('create', 'Drupal\community_exchange\Form\CreateExchange');
      $entity_types['group']->setLinkTemplate('contact-form', '/group/{group}/contact');
    }
  }
}

/**
 * Implements hook_entity_presave().
 *
 * Ensures new entities with new wallets have a wallet-type determined.
 */
function community_exchange_entity_presave($entity) {
  if (class_exists('\Drupal\mcapi\Mcapi'))  {//doesn't exist during installation
    if (\Drupal\mcapi\Mcapi::maxWalletsOfBundle($entity->getEntityTypeId(), $entity->bundle())) {
      drupal_set_message("Picked random wallet type for " .$entity->getEntityTypeId() ." because none specified");
    }
  }
}

/**
 * Implements hook_mcapi_wallet_insert().
 *
 * Set the wallet type, usually from the parent entity
 *
 * @note This hook unfortunately runs when the first wallet is created in
 * mcapi.install which is a dependency of this profile, thus the extra wallet
 * type vocabulary field hasn't been created and this hook shouldn't even be
 * loaded!
 */
function community_exchange_mcapi_wallet_insert($wallet) {
  $holder = $wallet->getHolder();
  if (isset($holder->walletType)) {
    $wallet->type->entity_id = $holder->walletType;
  }
  else {
    // Ignore the first two wallets being created during dependency installation
    if ($wallet->id() <= 2) {
      return;
    }
    $type_terms = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', 'wallet_types')
      ->execute();
    $wallet->type->entity_id = $type_terms[array_rand($type_terms)];
  }
}

/**
 * Implements hook_menu_links_discovered_alter().
 *
 * Disable unwanted menu items put in the main menu by core modules.
 *
 * @note this disabling can be re-enabled by user 1
 */
function community_exchange_menu_links_discovered_alter(&$links) {
//  $links['smallads.autolinks:offer.add_form.link']['weight'] = 6;
//  $links['smallads.autolinks:want.add_form.link']['weight'] = 7;
  $links['mcapi.mass']['enabled'] = FALSE;
  $links['devel_generate.exchange']['enabled'] = FALSE;
  $links['mcapi.transaction.add_form']['enabled'] = FALSE;
}

/**
 * Implements hook_mail().
 */
function community_exchange_mail($key, &$message, $params) {
  //$key always equals 'contact' from Drupal\mcapi_exchanges\Form\Contact
  return [
    'subject' => t(
      'Message from @site_name',
      ['@site_name' => \Drupal::config('system.site')->get('site.name')]
    ),
    'body' => [
      t(
        '@sender (@mail) wrote to you.',
        ['@sender' => $params['sender']->name, '@mail' => $params['sender']->mail]
      ),
      Drupal\Core\Mail\MailFormatHelper::htmlToText($params['message'])
    ]
  ];
}

/**
 * Implements hook_block_view_BASE_BLOCK_ID_alter().
 *
 * Add some non-group operations to the exchange operations block
 */
function community_exchange_block_view_exchange_operations_alter(&$build, \Drupal\Core\Block\BlockPluginInterface $block) {
  $build['#pre_render'][] = 'community_exchange_extra_operations';
}
function community_exchange_extra_operations($build) {
  if (isset($build['content']['#exchange'])) {
    if (GroupAccessResult::allowedIfHasGroupPermission($build['content']['#exchange'], \Drupal::currentUser(), 'manage transactions')) {
      $build['content']['#items'][] = [
        '#type'=> 'link',
        '#title' => t('Manage currencies'),
        '#url' => \Drupal\Core\Url::fromRoute('entity.mcapi_currency.collection')
      ];
    }
  }
  return $build;
}


/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add a filter to show only wallets in a given exchange
 */
function community_exchange_form_transaction_stats_filter_alter(&$form, $form_state) {
  // Get all the exchanges using this currency.
  $exchanges = \Drupal::entityQuery('group')
    ->condition('type', 'exchange')
    ->condition('currencies', $form_state->get('currency')->id())
    ->execute();
  if (count($exchanges) == 1) {
    foreach (Group::loadMultiple($exchanges) as $exchange) {
      $options[$exchange->id()] = $exchange->label();
    }
    $form['exchange'] = [
      '#title' => t('Exchange'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => isset($_SESSION['transaction_stats_filter']) ? $_SESSION['transaction_stats_filter']['exchange'] : '',
      '#empty_option' => t('- All -'),
      '#required' => FALSE,
    ];
    $form['#submit'][] = 'community_exchange_transaction_stats_filter_submit';
  }
}

/**
 * Submit callback for transaction_stats_filter form
 */
function community_exchange_transaction_stats_filter_submit($form, $form_state) {
  if (isset($_SESSION['transaction_stats_filter']['exchange'])) {
    $exid = $_SESSION['transaction_stats_filter']['exchange'];
    $_SESSION['transaction_stats_filter']['wallet_id'] = Exchanges::walletsInExchanges([$exid]);
  }
}

/**
 * Implements hook_ENTITY_TYPE_build_defaults_alter().
 */
function community_exchange_mcapi_currency_build_defaults_alter(&$build, $entity, $view_mode) {
  if (!empty($_SESSION['transaction_stats_filter']['exchange'])) {
    $exchange = Group::load($_SESSION['transaction_stats_filter']['exchange']);
    $build['#title'] = t(
      '%currency used in %exchange',
      ['%currency' => $entity->label(), '%exchange' => $exchange->label()]
    );
  }
}

/**
 * Utility to be moved to ggroup module.
 *
 * @param \Drupal\group\Entity\GroupInterface $parent_group
 * @param string[] $subgroup_types
 *
 * @return GroupContent[]
 */
function ggroup_get_subgroups(\Drupal\group\Entity\GroupInterface $parent_group, $subgroup_types = []) {
  $query = \Drupal::EntityQuery('group_content')
    ->condition('gid', $parent_group->id());
  // If no types have been passed then get the ids of all installed subgroup plugins.
  if (empty($subgroup_types)) {
    $subgroup_types = array_keys(\Drupal\group\Entity\GroupContentType::loadByEntityTypeId('group'));
  }
  $query->condition('type', $subgroup_types, 'IN');

  return \Drupal\group\Entity\GroupContent::loadMultiple($query->execute());
}

/**
 * Implements hook_entity_extra_fields().
 */
function community_exchange_entity_extra_field_info() {
  $info = [
    'group' => [
      'exchange' => [
        'display' => [
          'neighbourhoods' => [
            'label' => t('Neighbourhoods'),
            'description' => t("A list of neighbourhoods in the current user's exchange."),
            'weight' => 6,
          ],
          'local_projects' => [
            'label' => t('Local projects'),
            'description' => t("A list of local projects in the current user's exchange."),
            'weight' => 6,
          ]
        ]
      ]
    ]
  ];
}

/**
 * Implements hook_ENTITY_TYPE_view();
 */
function community_exchange_group_view(array &$build, $entity, $display, $view_mode) {
  if ($entity->bundle() === 'exchange') {
    if ($info = $display->getComponent('neighbourhoods')) {
      $build['neighbourhoods'] = ggroup_build_subgroup_list($entity, ['exchange-subgroup-neighbourhood'], t('Neighbourhoods'));
    }
    if ($info = $display->getComponent('local_projects')) {
      $build['local_projects'] = ggroup_build_subgroup_list($entity, ['exchange-subgroup-local_project'], t('Local projects'));
    }
  }
}

/**
 * A renderable list of the subgroups of a group.
 *
 * @param GroupInterface $group
 *   The parent group.
 * @param array $sub_group_types
 *   The group type or types of the subgroups to list
 * @param string $title
 *   The title of the item_list
 *
 * @return []
 *   a renderable array
 */
function ggroup_build_subgroup_list(GroupInterface $group, array $sub_group_types, $title = '') {
  // Make an ordered list of subgroups
  $items = $build = [];
  foreach (ggroup_get_subgroups($group, $sub_group_types) as $group_content) {
    $items[] = $group_content->getEntity()->link();
  }
  if ($items) {
    $build = [
      '#theme' => 'item_list',
      '#title' => $title,
      '#type' => 'ul',
      '#items' => $items
    ];
  }
  return $build;
}

/**
 * Identify and load an exchange by its unique code, NOT the db key
 *
 * @todo add this field and use it in the path alias
 */
function exchangeLoadByCode($code) {
  $exchanges = \Drupal::entityTypeManager()
    ->getStorage('mcapi_exchange')
    ->loadByProperties(['code' => $code]);
  return reset($exchanges);
}