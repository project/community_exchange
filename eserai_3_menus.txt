This is an attempt to collate all the menu links into separate menus.
To see the submenu links (by role) see the previous doc.
It is just about organising things in a coherent way, not about the user experience.

main-menu
- wall
- small ads
- transactions including analytics
- people

user-menu
- My account
- Edit profile
- My settings
- My Ads
- My groups //@todo
- My transactions
- Printable
- Downloads
- Log out

group action links Block
- Bill another member
- Credit another member
- Add Offer
- Add Want
- New wall post
- |mass payment
- |New local project
- |New neighbourhood
- |Administer categories
- |Edit exchange [group admin only]


tools-menu (for Committee members)
- Filter/list wallets in the group
- Filter/list users in the group, including unapproved members
- list/edit/delete reccommendations (comments on users)
- list/edit delete smallads //wait till group content is properly displayed before thinking about this
- Show currencies
- Create 3rd party transaction / mass transaction
+ Create member
+ create neighbourhood subgroup
+ Create currency [group admin only]
- Templates [group admin only]

Admin menu for user 1 only
- Default Templates
- List exchanges showing login count, transaction count, open, balance of trade, with filters, sorts and 'Approve New' operation
- set global levy rate
- Global analytics
+ Add new exchange

