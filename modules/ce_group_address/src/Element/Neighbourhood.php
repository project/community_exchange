<?php

namespace Drupal\ce_group_address\Element;

use Drupal\group\Entity\Group;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;

/**
 * Provides a form element for selecting a Neighbourhood from the neighbourhoods
 * from the given (user) entity's exchange.
 *
 * @FormElement("neighbourhood")
 */
class Neighbourhood extends Select {

  /**
   * {@inheritdoc}
   */
  public static function processSelect(&$element, FormStateInterface $form_state, &$complete_form) {
    if (!isset($element['#exchange_id'])) {
      throw new \Exception('Neighbourhood widget requires a group id.');
    }
    //assumes the gid is for a group of type exchange
    $neighbourhoods = \Drupal::entityTypeManager()
      ->getStorage('group_content')
      ->loadByProperties(['type' => 'exchange-subgroup-neighbourhood', 'gid' => $element['#exchange_id']]);

    if (empty($neighbourhoods)) {
      $group = Group::load($element['#exchange_id']);
      if ($group->getGroupType()->id() == 'exchange') {
        drupal_set_message(t('No neighbourhoods in exchange %group_name', ['%group_name' => $group->label()]), 'warning');
        return [];
      }
      else {
        throw new \Exception('Neighbouhood element expected group of type exchange, but got '.$group->getGroupType()->id());
      }
    }
    foreach ($neighbourhoods as $hood) {
      $element['#options'][$hood->label()] = $hood->label();
    }

    unset($element['#size']);
    return parent::processSelect($element, $form_state, $complete_form);
  }

}
