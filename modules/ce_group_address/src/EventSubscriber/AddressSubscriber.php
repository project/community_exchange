<?php

namespace Drupal\ce_group_address\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Eserai event subscriber.
 */
class AddressSubscriber implements EventSubscriberInterface {

  /**
   * Ensure every address format has %dependentLocality, sticking it on the end
   * if necessary, that that field is required, and called neighbourhood.
   *
   * @todo look out for a way to save country settings properly
   */
  public function rewriteAddress(Event $event, $event_name, $dispatcher) {
    $addressFormat = $event->getDefinition();
    if (!strpos($addressFormat['format'], '%dependentLocality')) {
      $addressFormat['format'] .= "\n%dependentLocality";
    }
    $addressFormat['dependent_locality_type']= 'neighborhood';
    $addressFormat['required_fields'] = ['locality'];
    $addressFormat['required_fields'] = array_unique($addressFormat['required_fields']);
    $event->setDefinition($addressFormat);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events['address.address_format'] = [['rewriteAddress']];

    return $events;
  }

}
