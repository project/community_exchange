api = 2
core = 8.x

projects[admin_toolbar] = "8.x-1.x-dev"
projects[captcha] = "8.x-1.x-dev"
projects[composer_manager] = "8.x-1.x-dev"
projects[country] = "8.x-1.x-dev"
projects[devel] = "8.x-1.x-dev"
;projects[l10n_client] = ""
projects[mailsystem] = "8.x-4.x-dev"
projects[mutual_credit] = "8.x-4.x-dev"
projects[rules] = "8.x-3.x-dev"
projects[typed_data] = "8.x-1.x-dev"
;projects[queue_mail] = ""
projects[smallads] = "8.x-1.x-dev"


